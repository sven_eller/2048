package ee.progmatic.numbergame.engine;

public enum Move {
    UP {
        @Override
        MoveProcessor getMoveProcessor() {
            return new MoveUp();
        }
    },
    DOWN {
        @Override
        MoveProcessor getMoveProcessor() {
            return new MoveDown();
        }
    },
    RIGHT {
        @Override
        MoveProcessor getMoveProcessor() {
            return new MoveRight();
        }
    },
    LEFT {
        @Override
        MoveProcessor getMoveProcessor() {
            return new MoveLeft();
        }
    };
    abstract MoveProcessor getMoveProcessor();
}

abstract class MoveProcessor{
    public void move(GameBoard gameBoard){
        for (int i = 1; i <= gameBoard.getBoardSize(); i++) {
            collapseAndMergeLine(i, gameBoard);
        }
    }
    abstract void collapseAndMergeLine(int i, GameBoard gameBoard);
}

class MoveRight extends MoveProcessor {
    @Override
    void collapseAndMergeLine(int i, GameBoard gameBoard) {
        int[] row = gameBoard.getRow(i);
        GameUtils.rightCollapseAndMerge(row);
        gameBoard.replaceRow(i, row);
    }
}

class MoveLeft extends MoveProcessor {
    @Override
    void collapseAndMergeLine(int i, GameBoard gameBoard) {
        int[] row = gameBoard.getRow(i);
        GameUtils.leftCollapseAndMerge(row);
        gameBoard.replaceRow(i, row);
    }
}

class MoveDown extends MoveProcessor {
    @Override
    void collapseAndMergeLine(int i, GameBoard gameBoard) {
        int[] column = gameBoard.getColumn(i);
        GameUtils.rightCollapseAndMerge(column);

        gameBoard.replaceColumn(i, column);
    }
}

class MoveUp extends MoveProcessor {
    @Override
    void collapseAndMergeLine(int i, GameBoard gameBoard) {
        int[] column = gameBoard.getColumn(i);
        GameUtils.leftCollapseAndMerge(column);
        gameBoard.replaceColumn(i, column);
    }
}
