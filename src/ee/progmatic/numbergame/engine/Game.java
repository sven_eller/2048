package ee.progmatic.numbergame.engine;

import static ee.progmatic.numbergame.engine.Game.State.ONGOING;
import static ee.progmatic.numbergame.engine.Game.State.WIN;

public class Game {

    public static final int WINNING_SCORE = 2048;
    private static final int EMPTY_CELL_VALUE = 0;
    private static final int BOARD_SIZE = 4;

    private GameBoard gameBoard = new GameBoard(BOARD_SIZE);
    private NewCellMaker newCellMaker;

    public Game() {
        newCellMaker = new RandomNumberToEmptyCellAdder();
    }

    public void startGame(){
        newCellMaker.addNumberToEmptyCell();
        newCellMaker.addNumberToEmptyCell();
    }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    public void makeMove(Move move){
        move.getMoveProcessor().move(gameBoard);
        newCellMaker.addNumberToEmptyCell();

    }

    public State gameState(){
        if(gameBoard.hasValueOnBoard(WINNING_SCORE)){
            return WIN;
        }
        if(gameBoard.hasCellsThatCanBeMerged() || gameBoard.hasValueOnBoard(EMPTY_CELL_VALUE)){
            return ONGOING;
        }
        return State.LOST;
    }

    void setGameBoard(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
    }

    void setNewCellMaker(NewCellMaker newCellMaker) {
        this.newCellMaker = newCellMaker;
    }

    public enum State{
        WIN, LOST, ONGOING
    }

    class RandomNumberToEmptyCellAdder implements NewCellMaker {
        public void addNumberToEmptyCell(){

        }
    }
}
