package ee.progmatic.numbergame.engine;

import java.util.LinkedList;

class GameUtils {

    /**
     *  Gets array as input, collapses it to right, merges same values next to each other by adding them, and collapses it again.
     *  Ie. [2, 2, 2, 32] becomes [0, 2, 4, 32]
      * @param arrayToMerge - Array that is merged
     */
    public static void rightCollapseAndMerge(int[] arrayToMerge){
        collapseRight(arrayToMerge);
        mergeArrayToRight(arrayToMerge);
        collapseRight(arrayToMerge);
    }

    /**
     *  Gets array as input, collapses it to left, merges same values next to each other by adding them, and collapses it again.
     *  Ie. [2, 2, 2, 32] becomes [4, 2, 32, 0]
     * @param arrayToMerge - Array that is merged
     */
    public static void leftCollapseAndMerge(int[] arrayToMerge){
        collapseLeft(arrayToMerge);
        mergeArrayToLeft(arrayToMerge);
        collapseLeft(arrayToMerge);
    }

    static void collapseRight(int[] arrayToCollapse) {
        LinkedList<Integer> newCollection = new LinkedList<Integer>();
        for(int value : arrayToCollapse){
            if(value != 0){
                newCollection.addLast(value);
            }
        }
        while (newCollection.size() < arrayToCollapse.length){
            newCollection.addFirst(0);
        }
        replaceArrayWithCollection(arrayToCollapse, newCollection);
    }

    private static void replaceArrayWithCollection(int[] arrayToCollapse, LinkedList<Integer> newCollection) {
        for(int i = arrayToCollapse.length - 1; i >= 0; i--){
            arrayToCollapse[i] = newCollection.get(i);
        }
    }

    static void collapseLeft(int[] arrayToCollapse) {
        LinkedList<Integer> newCollection = new LinkedList<>();
        for(int i = arrayToCollapse.length -1 ; i >= 0; i--){
            int value = arrayToCollapse[i];
            if(value != 0){
                newCollection.addFirst(value);
            }
        }
        while (newCollection.size() < arrayToCollapse.length){
            newCollection.addLast(0);
        }

        replaceArrayWithCollection(arrayToCollapse, newCollection);
    }

    static void mergeArrayToRight(int[] initial) {
        for(int i = initial.length  - 2; i >= 0; i--){
            if(initial[i] != 0 && initial[i] == initial[i + 1]){
                initial[i+1] = initial[i] + initial[i + 1];
                initial[i] = 0;
            }
        }
    }

    static void mergeArrayToLeft(int[] initial) {
        for(int i = 1; i < initial.length - 1; i++){
            if(initial[i] != 0 && initial[i] == initial[i - 1]){
                initial[i - 1] = initial[i] + initial[i - 1];
                initial[i] = 0;
            }
        }
    }
}
