package ee.progmatic.numbergame.engine;

interface NewCellMaker {
    public void addNumberToEmptyCell();
}
