package ee.progmatic.numbergame.engine;

import java.util.Arrays;

/**
 * Object to hold and modify game state.
*/
class GameBoard {


    private int[][] gameBoard;
    private  int boardSize;

    GameBoard(int boardSize){
        gameBoard = new int[boardSize][boardSize];
        this.boardSize = gameBoard.length;
    }

    GameBoard(int[][] gameBoard) {
        this.gameBoard = gameBoard;
        this.boardSize = gameBoard.length;
    }

    public int getBoardSize() {
        return gameBoard.length;
    }

    int[][] getGameBoard(){
        int[][] boardCopy = new int[boardSize][boardSize];
        for(int i = 1; i <= boardSize; i++){
            boardCopy[i - 1] = getRow(i);
        }
        return boardCopy;
    }

    public int[] getRow(int rowNumber){
        if(rowNumber <= 0 || rowNumber > boardSize){
            throw new IllegalArgumentException("Row number must be in range of [ 1 .." + "" + boardSize + "] ... but given = " + rowNumber );
        }
        int[] row = gameBoard[rowNumber - 1];
        return Arrays.copyOf(row, row.length);
    }

    public void replaceRow(int number, int[] replacement){
        gameBoard[number - 1] = Arrays.copyOf(replacement, replacement.length);
    }

    public void addToBoard(int row, int column, int value) {
        if(gameBoard[row][column] != 0){
            throw new IllegalStateException("Cell already has value");
        }
        gameBoard[row][column] = value;
    }

    public boolean hasCellsThatCanBeMerged(){
        for(int x = 0; x < boardSize; x++){
            for(int y = 0; y < boardSize; y++){

                int boardCell = gameBoard[x][y];

                if(x != boardSize - 1 && gameBoard[x + 1][y] == boardCell){
                    return  true;
                }

                if(y != boardSize - 1 && gameBoard[x][y + 1] == boardCell){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasValueOnBoard(int valueToLook){
        for(int x = 0; x < boardSize; x++){
            for(int y = 0; y < boardSize; y++){
                int boardCell = gameBoard[x][y];
                if(boardCell == valueToLook){
                    return true;
                }
            }
        }
        return false;
    }

    int[][] getColumns() {
        int[][] colSet = new int[boardSize][boardSize];
        for(int row = 0; row < boardSize; row++){
            for(int col = 0; col < boardSize; col++){
                colSet[col][row] = gameBoard[row][col];
            }
        }
        return colSet;
    }

    public int[] getColumn(int i){
        return getColumns()[i - 1];
    }

    public void replaceColumn(int i, int[] replacement) {
        for(int row = 0; row < boardSize; row ++){
            gameBoard[row][i - 1] = replacement[row];
        }
    }
}
