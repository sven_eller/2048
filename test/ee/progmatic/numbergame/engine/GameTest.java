package ee.progmatic.numbergame.engine;


import org.junit.Before;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class GameTest {
    Game game;

    @Before
    public void setUp(){
        game = new Game();
    }

    @Test
    public void gameIsWon(){
        int[][] gameBoard = new int[4][4];
        gameBoard[0][0] = 2048;
        game.setGameBoard(new GameBoard(gameBoard));
        assertThat(game.gameState(), equalTo(Game.State.WIN));
    }

    @Test
    public void gameIsLost(){
        int[][] gameBoard = {
                {2, 4, 8, 16},
                {32, 64, 128, 256},
                {2, 4, 8, 16},
                {32, 64, 128, 256},

        };
        game.setGameBoard(new GameBoard(gameBoard));
        assertThat(game.gameState(), equalTo(Game.State.LOST));
    }

    @Test
    public void gameIsOnGoingWhenHaveCellsToMergeFromRight(){
        int[][] gameBoard = {
                {2, 2, 8, 16},
                {32, 64, 128, 256},
                {2, 4, 8, 16},
                {32, 64, 128, 256},

        };
        game.setGameBoard(new GameBoard(gameBoard));
        assertThat(game.gameState(), equalTo(Game.State.ONGOING));
    }

    @Test
    public void gameIsOnGoingWhenHaveCellsToMergeFromDown(){
        int[][] gameBoard = {
                {2, 4, 8, 16},
                {2, 64, 128, 256},
                {2, 4, 8, 16},
                {32, 64, 128, 256},

        };
        game.setGameBoard(new GameBoard(gameBoard));
        assertThat(game.gameState(), equalTo(Game.State.ONGOING));
    }

    @Test
    public void gameIsOnGoingWhenHavingUnusedCells(){
        int[][] gameBoard = {
                {0, 0, 0, 2},
                {2, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},

        };
        game.setGameBoard(new GameBoard(gameBoard));
        assertThat(game.gameState(), equalTo(Game.State.ONGOING));
    }

    @Test
    public void createTwoNumbersOnGameCreation(){
        Game game = new Game();
        MockNewCellMaker numberGenerator = new MockNewCellMaker(game);
        game.setNewCellMaker(numberGenerator);

        numberGenerator.fixNewNumberPositionTo(0,0,2);
        numberGenerator.fixNewNumberPositionTo(0,1,2);

        game.startGame();


        assertEquals(2, game.getGameBoard().getGameBoard()[0][0]);


        assertEquals(2, game.getGameBoard().getGameBoard()[0][1]);
    }

    @Test
    public void createNumberOnMove(){
        assertEquals(game.getGameBoard().getGameBoard()[0][2], 0);
        Game game = new Game();

        MockNewCellMaker numberGenerator = new MockNewCellMaker(game);
        numberGenerator.fixNewNumberPositionTo(0,0,2);
        numberGenerator.fixNewNumberPositionTo(0,1,2);
        game.setNewCellMaker(numberGenerator);
        game.startGame();

        int boardAfterInit[][]  = {
            {2, 2, 0, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        };

        assertTrue(
                Arrays.deepEquals(game.getGameBoard().getGameBoard(),  boardAfterInit)
        );


        numberGenerator.fixNewNumberPositionTo(0,2, 2);

        game.makeMove(Move.DOWN);

        int boardAfterMove[][]  = {
                {0, 0, 2, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {2, 2, 0, 0},
        };

        assertTrue(
                Arrays.deepEquals(game.getGameBoard().getGameBoard(),  boardAfterMove)
        );
    }

    @Test
    public void cellsMovedRight(){
        Game game = new Game();

        int[][] beforeMove = {
                {2, 4, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {2, 0, 0, 2},

        };
        game.setGameBoard(new GameBoard(beforeMove));

        int[][] afterMove = {
                {0, 0, 2, 4},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 4},

        };

        game.makeMove(Move.RIGHT);

        assertTrue(
                Arrays.deepEquals(game.getGameBoard().getGameBoard(), afterMove)
        );
    }

    @Test
    public void cellsMovedLeft(){
        Game game = new Game();

        int[][] beforeMove = {
                {2, 4, 4, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {2, 0, 0, 2},

        };
        game.setGameBoard(new GameBoard(beforeMove));

        int[][] afterMove = {
                {2, 8, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {4, 0, 0, 0},

        };

        game.makeMove(Move.LEFT);

        assertTrue(
                Arrays.deepEquals(game.getGameBoard().getGameBoard(),  afterMove)
        );
    }

    @Test
    public void cellsAreMergedOnMoveDown(){
        Game game = new Game();

        int[][] beforeMove = {
                {2, 4, 8, 16},
                {2, 64, 128, 256},
                {2, 4, 8, 16},
                {32, 64, 128, 256},

        };
        game.setGameBoard(new GameBoard(beforeMove));

        int[][] afterMove = {
                {0, 4, 8, 16},
                {2, 64, 128, 256},
                {4, 4, 8, 16},
                {32, 64, 128, 256},

        };
        game.makeMove(Move.DOWN);
        assertTrue(
                Arrays.deepEquals(game.getGameBoard().getGameBoard(), afterMove)
        );

    }

    @Test
    public void cellsAreMergedOnMoveUp(){
        Game game = new Game();

        int[][] beforeMove = {
                {4, 4, 8, 16},
                {32, 64, 128, 256},
                {32, 4, 8, 16},
                {0, 64, 128, 256},

        };
        game.setGameBoard(new GameBoard(beforeMove));

        int[][] afterMove = {
                {4, 4, 8, 16},
                {64, 64, 128, 256},
                {0, 4, 8, 16},
                {0, 64, 128, 256},

        };
        game.makeMove(Move.UP);
        assertTrue(
                Arrays.deepEquals(game.getGameBoard().getGameBoard(), afterMove)
        );
    }
}

class MockNewCellMaker implements NewCellMaker {

    Queue<CellValue> nextCellsToBeFilled = new ArrayDeque<CellValue>() {};

    Game game;

    MockNewCellMaker(Game game) {
        this.game = game;
    }

    public void fixNewNumberPositionTo(int row, int column, int value){
        nextCellsToBeFilled.add(new CellValue(row, column, value));
    }

    public void addNumberToEmptyCell() {
        if(!nextCellsToBeFilled.isEmpty()){
            CellValue cellValue = nextCellsToBeFilled.poll();
            game.getGameBoard().addToBoard(cellValue.row, cellValue.column, cellValue.value);
        }
    }
}

class CellValue {
    int row, column, value;

    CellValue(int row, int column, int value) {
        this.row = row;
        this.column = column;
        this.value = value;
    }
}



