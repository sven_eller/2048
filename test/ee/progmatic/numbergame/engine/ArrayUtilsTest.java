package ee.progmatic.numbergame.engine;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class ArrayUtilsTest {

    @Test
    public void shiftArraysToRight() {
        int[] initial = {2, 4, 0, 0};
        int[] expected = {0, 0, 2, 4};
        GameUtils.collapseRight(initial);
        System.out.print(Arrays.toString(initial));
        assertArrayEquals(expected, initial);
    }

    @Test
    public void collapseArraysToRight() {
        int[] initial = {2, 0, 0, 4};
        int[] result = {0, 0, 2, 4};
        GameUtils.collapseRight(initial);
        System.out.print(Arrays.toString(initial));
        assertArrayEquals(result, initial);
    }

    @Test
    public void collapseArraysToLeft(){
        int[] initial = {2, 0, 0, 4};
        int[] result = {2, 4, 0, 0};
        GameUtils.collapseLeft(initial);
        System.out.print(Arrays.toString(initial));
        assertArrayEquals(result, initial);
    }

    @Test
    public void mergeArraysToRight() {
        int[] initial = {2, 2, 2, 32};
        int[] result = {2, 0, 4, 32};
        GameUtils.mergeArrayToRight(initial);
        System.out.print(Arrays.toString(initial));
        assertArrayEquals(result, initial);
    }

    @Test
    public void mergeArraysToLeft(){
        int[] initial = {2, 2, 2, 32};
        int[] result = {4, 0, 2, 32};
        GameUtils.mergeArrayToLeft(initial);
        System.out.print(Arrays.toString(initial));
        assertArrayEquals(result, initial);
    }
}
