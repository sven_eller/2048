package ee.progmatic.numbergame.engine;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertTrue;

public class GameBoardTest {
    @Test
    public void getColumns(){

        int[][] initialBoard = {
                {2, 4, 8, 16},
                {2, 64, 128, 256},
                {2, 4, 8, 16},
                {32, 64, 128, 256}
        };
        GameBoard gameBoard = new GameBoard(initialBoard);

        int[][] asColumns = {
                {2, 2, 2, 32},
                {4, 64, 4, 64},
                {8, 128, 8, 128},
                {16, 256, 16, 256}
        };
        int[][] result = gameBoard.getColumns();
        assertTrue(
                Arrays.deepEquals(result, asColumns)
        );
    }

    @Test
    public void replaceColumn(){
        int[][] initialBoard = {
                {2, 4, 8, 16},
                {2, 64, 128, 256},
                {2, 4, 8, 16},
                {32, 64, 128, 256}
        };
        GameBoard gameBoard = new GameBoard(initialBoard);

        int[] replacement = {1, 2, 3, 4};
        gameBoard.replaceColumn(1, replacement);


        int[] result = gameBoard.getColumn(1);
        assertTrue(
                Arrays.equals(result, replacement)
        );
    }
}
